<?php

namespace App\Tests;

use App\Entity\Demo;
use PHPUnit\Framework\TestCase;

class UnitTestDemoTest extends TestCase
{
    public function testDemo()
    {
        $demo = New Demo();
        $demo->setName('demo');
        $this->assertTrue($demo->getName() === 'demo');
    }
}
